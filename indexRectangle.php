<?php
function __autoload($classname)
{
    include_once $classname . ".php";
}


$rectangle1 = new Rectangle();
$rectangle1->width = 100;
$rectangle1->length = 200;


$rectangle2 = new Rectangle();
$rectangle2->width = 35;
$rectangle2->length = 67;

$displayer1 = new Displayer();

$displayer1->displaypre($rectangle1->getArea());
$displayer1->displayh1($rectangle2->getArea());
