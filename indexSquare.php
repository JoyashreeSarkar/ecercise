<?php
function __autoload($classname)
{
    include_once $classname . ".php";
}


$square1 = new Square();
$square1->length = 20;


$square2 = new Square();
$square2->length = 100;

$displayer1 = new Displayer();

$displayer1->displaypre($square1->getArea());
$displayer1->displayh1($square2->getArea());
