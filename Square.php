<?php

class Square
{
    public $length;

    public function getArea()
    {
        return $this->length * $this->length;
    }

}