<?php
function __autoload($classname)
{
    include_once $classname . ".php";
}


$volume1 = new Volume();
$volume1->width = 100;
$volume1->length = 200;
$volume1->height = 50;


$volume2 = new Volume();
$volume2->width = 3;
$volume2->length = 3;
$volume2->height = 3;

$displayer1 = new Displayer();

$displayer1->displaypre($volume1->getArea());
$displayer1->displayh1($volume2->getArea());
