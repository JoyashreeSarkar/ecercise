<?php
class Rectangle
{
    public $width;
    public $length;

    public function getArea()
    {
        return $this->length * $this->width;
    }
}